import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Notes from './components/Notes';

class App extends Component {
  notes = [];
  state = {
    title: "To Do App",
    notes: [],
    selectAll: false,
    selectAllButtonVisible : true
  };

  

  addNote(newNote) {
    var tempArray = this.state.notes.slice();
    if (newNote.trim() != "") {
      tempArray.push({
        text: newNote.trim(),
        completed: false
      });

      this.setState({ notes: tempArray });
      this.notes = tempArray;
      
      document.getElementById("input").value = "";
    
    }  

  }

  toggle(note) {
    // console.log("id " + note);
    var tempArray = this.state.notes.slice();
    if (tempArray[note].completed) {
      tempArray[note].completed = false;
    } else {
      tempArray[note].completed = true;
    }

    //if all notes are checked we are changing the state of select all checkbox to checked
    this.setState({ notes: tempArray , selectAll: (this.checkCompleted(tempArray).length == tempArray.length) ? true : false});
    this.notes = tempArray;

    


    


  }
  
  checkCompleted(array) {
    var completeArray = array.filter(function (element) {
      return element.completed === true;
    });

    return completeArray;
  }

  deleteNote(id) {
    id = parseInt(id);
    var tempArray = this.state.notes.slice();
    tempArray.splice(id, 1);
    this.setState({ notes: tempArray });
    this.notes = tempArray;



  }

  editNote(id, note) {
    id = parseInt(id);
    
    var tempArray = this.state.notes.slice();
    note = note.trim();
    if (note !== "") {
            
      tempArray[id].text = note;
      
      this.setState({ notes: tempArray });

    
      
    } else {
       //workaround to force react to update the component and avoid showing blank note 
       tempArray[id].text = (tempArray[id].text+" ");
       this.setState({ notes: tempArray });
      
      
    }

  }

  showAll() {
    this.setState({ notes: this.notes , selectAllButtonVisible: true });

    


  }

  showActive() {
    var tempArray = this.notes.slice();
    var activeArray = tempArray.filter(function (element) {
      return element.completed === false;
    });

    

    this.setState({ notes: activeArray, selectAllButtonVisible: (activeArray.length == 0) ? false: true });

    
   


  }

  showCompleted() {
    var tempArray = this.notes.slice();
 
    var completedArray = tempArray.filter(function (element) {
      return element.completed === true;
    });
   

    this.setState({ notes: completedArray, selectAllButtonVisible: (completedArray.length == 0) ? false: true  });
    
    
  }

  selectAll() {

    
    var tempArray = this.state.notes.slice();
    tempArray.forEach(function(element) {
      (this.state.selectAll) ? element.completed = false : element.completed = true;
    }, this);
    
    

    
    this.setState({ notes: tempArray , selectAll: (this.state.selectAll) ? false : true });

    
    
    
  }
  render() {
    

    return (
      <div id="container">
        <Header selectAllButtonVisible={this.state.selectAllButtonVisible} selectAllButtonChecked={this.state.selectAll} selectAll={this.selectAll.bind(this)} showAll={this.showAll.bind(this)} showActive={this.showActive.bind(this)} showCompleted={this.showCompleted.bind(this)} addNote={this.addNote.bind(this)} title={this.state.title} />
        <Notes  editNote={this.editNote.bind(this)} deleteNote={this.deleteNote.bind(this)} togglenote={this.toggle.bind(this)} note={this.state.notes} />
      </div>
    );
  }




}



export default App;

