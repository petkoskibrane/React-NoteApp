import React, { Component } from 'react';


class Checkbox extends Component {

    toggle(event) {
        this.props.toggle(parseInt(event.target.id));
    }

    render() {
        return (
            <input
                    
                id= {this.props.id}    
                type="checkbox"
                checked={this.props.ischecked}
                onClick={this.toggle.bind(this)}/> 
            
        );
    }
}

export default Checkbox;