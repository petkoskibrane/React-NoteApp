import React, { Component } from 'react';


class DeleteButton extends Component {

    delete(event) {
        this.props.delete(parseInt(event.target.id));
    }

    render() {
        return (
            <i
                className="material-icons"    
                id= {this.props.id}
                onClick={this.delete.bind(this)}>delete</i> 
            
        );
    }
}

export default DeleteButton;