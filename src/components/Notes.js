import React, { Component } from 'react';
import Checkbox from '../components/Checkbox';
import DeleteButton from '../components/DeleteButton'

class Notes extends Component {
    togglecheckbox(id) {
        this.props.togglenote(id);
    }

    delete(id) {
        this.props.deleteNote(id);
    }

    makeEditable(event) {
        event.target.setAttribute("contentEditable", true);
    }

    editnote(event) {
        if (event.which === 13) {

            event.target.setAttribute("contentEditable", false);

            event.target.blur();

            this.props.editNote(parseInt(event.target.id), event.target.innerText);

        }
    }

  


    render() {
        return (
            <div>{this.props.note.map(function (value, index) {
                return <div className="note">
                    <Checkbox toggle={this.togglecheckbox.bind(this)} id={index + "checkbox"} ischecked={value.completed} />
                    <span  className={(value.completed) ? 'completed' : 'notcompleted'} id={index + "notespan"} onKeyPress={this.editnote.bind(this)} onClick={this.makeEditable.bind(this)} onMouseOver={this.makeEditable.bind(this)}>{value.text}</span>
                    <DeleteButton delete={this.delete.bind(this)} id={index + "deletenote"} />

                </div>
            }, this)}</div>
        );
    }
}

export default Notes;
