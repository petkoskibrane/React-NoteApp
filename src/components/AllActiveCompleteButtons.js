import React, { Component } from 'react';


class AllActiveCompleteButtons extends Component {



    state = {
        selected: 'All'
    };

    showAll() {
        this.props.showAll();
        this.setState({ selected: 'All' });
    }

    showActive(){
        this.props.showActive();
        this.setState({ selected: 'Active' });
        
    }

    showCompleted(){
        this.props.showCompleted();
        this.setState({ selected: 'Completed' });
        
    }

    

    render() {
        return (
            <div className="buttons-container">
                <input className={(this.props.selectAllButtonVisible)? "visible" : "hidden"} checked={this.props.selectAllButtonChecked} onClick={this.props.selectAll} id="selectAll" type="checkbox"/>
                <span className={ (this.state.selected == 'All') ? 'activeButton' : ''} onClick={this.showAll.bind(this)}>All</span>
                <span className={ (this.state.selected == 'Active') ? 'activeButton' : ''} onClick={this.showActive.bind(this)}>Active</span>
                <span className={ (this.state.selected == 'Completed') ? 'activeButton' : ''} onClick={this.showCompleted.bind(this)}>Completed</span>
            </div>

        );
    }
}

export default AllActiveCompleteButtons;