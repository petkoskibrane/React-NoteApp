import React, { Component } from 'react';
import AllActiveCompleteButtons from '../components/AllActiveCompleteButtons';


class Header extends Component {
    addNote(event) {
        // console.log(event.target.value);
        if (event.which === 13) {
            this.props.addNote(event.target.value);
        }     
    }
    render() {
        return (
            <div>
            <h1>{this.props.title}</h1> 
            <AllActiveCompleteButtons selectAllButtonVisible={this.props.selectAllButtonVisible} selectAllButtonChecked={this.props.selectAllButtonChecked} selectAll={this.props.selectAll} showAll={this.props.showAll} showActive={this.props.showActive} showCompleted={this.props.showCompleted} />
            <input placeholder="Buy a Coffee" id="input" onKeyPress={this.addNote.bind(this)} />
            </div>    
        );
    }
}

export default Header;